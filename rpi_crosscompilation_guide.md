# Cross Compiling for Raspberry Pi

The following steps can be followed to set up a cross compilation environment for Raspberry Pi.

##### Download tools repository

First, you need to download the official tools repository

```bash
git clone https://github.com/raspberrypi/tools.git
```

This repository contains different cross toolchains  that can be used to compile for the ARM processor. Also, it contains a basic sysroot, where compiled libraries and library headers can be found. 

```bash
/path/to/tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/arm-linux-gnueabihf/sysroot
```

However, to guarantee the same scenario as in the Raspbian image running on the Raspberry PI, it is required to synchronize this sysroot with the current one in the Raspberry Pi.

##### Synchonizing sysroot

To synchronize the sysroot, run the following command from the Raspberry Pi terminal:

```bash
rsync -avz --copy-unsafe-links -e 'ssh' /opt /lib /usr user@ip:/path/to/rpi/tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/arm-linux-gnueabihf/sysroot/
```

Notice that the correct path to the tools repository needs to be provided. Also, replace user for your user name in the host machine and the corresponding IP address. Your password, in the host machine, will be required to validate the connection through ssh.

**NOTE:** This process may take a while. So, please have some patience.

Once the sysroot has been synchronized, a couple of symbolic links are required (replace /path/to/pi/tools for the corresponding path to the tools folder recently cloned):

```bash
ln -s /path/to/pi/tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/arm-linux-gnueabihf/sysroot/usr/lib/arm-linux-gnueabihf /path/to/pi/tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/arm-linux-gnueabihf/sysroot/usr/lib/arm-linux-gnueabihf/4.9.3
```

```bash
ln -s /path/to/pi/tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/arm-linux-gnueabihf/sysroot/lib/arm-linux-gnueabihf /path/to/pi/tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/arm-linux-gnueabihf/sysroot/lib/arm-linux-gnueabihf/4.9.3
```

##### Download Eclipse for C++

Using Eclipse IDE is one of the easiest ways to cross compile for the Raspberry Pi. The following (recent) version of Eclipse for C++ has been tested.

https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2018-12/R/eclipse-cpp-2018-12-R-linux-gtk-x86_64.tar.gz 

Once the tarball is uncompressed, run eclipse.

```bash
tar -xvf eclipse-cpp-2018-12-R-linux-gtk-x86_64.tar.gz
cd eclipse
./eclipse
```

**Optional:** a symbolic link to this executable can be added to /usr/local/bin, so from any console/terminal eclipse can be executed.

```
sudo ln -s /path/to/eclipse/eclipse /usr/local/bin/eclipse 
```

Once running eclipse, and after selecting a directory as your workspace, from the welcome screen you can select "Create a new C++ project". Then, select a name for the project. In project type, you can select "Empty Project" and later add your source code. In Toolchains, select "Cross GCC" and click "Next". The following image summarizes this.

![image1](./image1.png)

Then, you can just select the "Debug" option and click "Next".

![image2](./image2.png)

In the following option window, you have to tell Eclipse which cross compile toolchain to use and where it is located. As depicted in the following image, you have to specify them as:

```bash
prefix=arm-linux-gnueabihf-
path=/path/to/tools/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin
```

providing the corresponding path to the cloned tools repository.

![image3](./image3.png)

Once the project is created, go to the project settings. For this, go to the project folder in the "Project Explorer" window. There, right-click over the project folder and select "Properties". You will see the following window:

![image4](./image4.png)

Then, go to "Settings" option for "C/C++ Build". There, as you can see in the following image, in "Cross Setting" you can see the previously provided prefix and path for the toolchain.

![image5](./image5.png)

Then, go to the "Libraries" option under "Cross G++ Linker". In the window "Libraries", add `pthread` and `wiringPi`, which is required for compiling the application.

![image6](./image6.png)

##### Connecting through SSH

For easily connecting to the Raspberry Pi using ssh, it is required that the Raspberry Pi and your host machine are in the same network. For this, a direct connection, using a network cable, can be used. First,                        you have to set a static IP address by running the following command in the Raspberry Pi's terminal:

```
ifconfig eth0 192.168.0.1 netmask 255.255.255.0 broadcast 255.255.255.255
```

Then, also you need to set a static IP address on you host machine.

```bash
ifconfig enp0s25 192.168.0.2 netmask 255.255.255.0 broadcast 255.255.255.255
```

Once this "network" is set up, you can connect to the Raspberry Pi using SSH

```bash
ssh pi@192.168.0.1
```

By doing so, the Raspberry Pi can be access through ssh and no monitor, mouse and keyboard will be required for using the Raspberry Pi or for triggering applications.

##### Mounting Raspeberry Pi file system (FS) on host machine

One easy way to move files and binaries to the Raspberry Pi is to mount its file system (FS) on your host machine. For these, you just need to install `sshfs` in your host machine and then run the following command:

```bash
sshfs pi@ipaddress: /path/where/to/mount
```

Notice that you need to provide the proper user (here pi) and ip address of the Raspberry Pi, and the path to the folder where to mount the FS. This process will ask for the password for the Raspberry Pi user.

Once the FS is mounted, any file which is move to the local folder, where the FS was mounted, will be transparent to the Raspberry Pi.
